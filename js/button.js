
class ButtonsListener{
    getClick(buttonId, callBack){
        const button = document.getElementById(buttonId)
        button.addEventListener('click', ()=>{
            callBack()
            const deleteButton = this.createButton('.hidden', 'deleteButton')
            this.deleteItem(deleteButton.id, '.hidden')
        })
    }

    deleteItem(buttonId, parent){
        const button = document.getElementById(buttonId)
        console.log(parent)
        button.addEventListener('click', ()=>{
            document.querySelector(parent).innerHTML = ''
        })
    }

    createButton(parentClass, buttonId){
        const parent = document.querySelector(parentClass) 
        const deleteButton = document.createElement('button')
        deleteButton.textContent = "Закрыть"
        deleteButton.id = buttonId
        parent.appendChild(deleteButton)
        return deleteButton
    }
}


export default ButtonsListener 

