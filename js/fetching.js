
const link = date => `http://api.openweathermap.org/data/2.5/${date}?lang=ru&q=Minsk,by&appid=494c4e06afc2c22fde2fe3f35a18e078`

class WeatherWidget{


    async getOneDayWeather(){
        const data = await fetch(link('weather'))
        .then(response => response.json())
        const newDiv =  document.createElement('div')
        newDiv.className = 'weather'
        const parent = document.querySelector(".hidden")
        const icon = data.weather[0]['icon']
        newDiv.innerHTML = `
        <span>${ `<img src="https://openweathermap.org/img/wn/${icon}@2x.png">`}</span>
        <span>${Math.round(data.main.temp - 273) + '&deg;'}</span>
        <span>${data.weather[0]['description']}</span>
        <span>${Math.round(data.wind.speed) + ' м/с'}</span>
        `
        parent.appendChild(newDiv)


    }

    async getThreeDaysWeather(){
        const data = await fetch(link('forecast'))
        .then(response => response.json())
        const parent = document.querySelector(".hidden")
        let counter = 0
        for(let i=0;i<data.list.length;i++){
            if(data.list[i].dt_txt.includes('12:00:00')){
                const newDiv =  document.createElement('div')
                newDiv.className = 'weather'
                const icon = data.list[i].weather[0]['icon']
                newDiv.innerHTML = `
                <span>${ `<img src="https://openweathermap.org/img/wn/${icon}@2x.png">`}</span>
                <span>${Math.round(data.list[i].main.temp - 273) + '&deg;'}</span>
                <span>${data.list[i].weather[0]['description']}</span>
                <span>${Math.round(data.list[i].wind.speed) + ' м/с'}</span>
                `
                parent.appendChild(newDiv)
                counter++
                console.log(data.list[i])
            }
            if(counter==3){
                break
            }
        }
    

    }

}

export default WeatherWidget

